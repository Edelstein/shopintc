import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import Navbar from '../Navbar/Navbar'
import Footer from '../parts/Footer/Footer'
import Pattern from '../../imgs/pattern.png'
import './help.css'

const Help = props => <div>
    <div className="shopin_background_results" />
    <Navbar />
    {window.scrollTo(0, 0)}
    <Container>
        <Row className="text-center justify-content-center">
            <Col xs="12">
                <h1 className="helpbot_title">Help bot</h1>
            </Col>
            <Col xs="12" className="">
                <p className="intro_text white" ><strong><a className="to_helpbot white" href="https://t.me/crowdnetworkbot?start=Shopin" target="_blank" rel="noopener">Click Here</a> and type /help in the chat.</strong></p>
             </Col>
        </Row>
        <div className="margin_top_180">
        <Footer className="margin_top_180"/>
        </div>

    </Container>
</div>

export default Help