import { combineReducers } from 'redux'
import users_reducer from './usersReducer'
import search_reducer from './searchReducer'
import url_reducer from './urlReducer'

export default combineReducers({
  users: users_reducer,
  search: search_reducer,
  came_from: url_reducer
})