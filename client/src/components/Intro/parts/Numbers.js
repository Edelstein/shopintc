import React from 'react'
import { Row, Col } from 'reactstrap'
import CountUp from 'react-countup'

const Numbers = props => <Row className="text-center margin_mobile_numbers margin_top_40 justify-content-center">
<Col xs="5" md="3" sm="4" className="margin_top_20 text-left opensans">
    <p>SHOPIN Made: {props.is_phone ? <br/> : null }<CountUp separator=',' start={0} end={props.tokens_collected} /></p>
</Col>
<Col md="4" className="margin_top_20 mobile_invisible opensans">
    <p className="mobile_invisible">Total Tokens: <CountUp separator=',' start={0} end={props.total_tokens} /></p>
</Col>
<Col xs="5" md="3" sm="4" className="margin_top_20 text-right opensans">
    <p>SHOPIN Left: {props.is_phone ? <br/> : null}<CountUp separator=',' start={0} end={props.tokens_left} /></p>
</Col>
</Row>

export default Numbers