import React, { Component } from 'react'
import { isEmail } from 'validator'
import axios from 'axios'
import Pattern from '../../imgs/pattern.png'
import Terms from '../../docs/terms.pdf'
import Navbar from '../Navbar/Navbar'
import Footer from '../parts/Footer/Footer'
import countries from '../../text/banned_countries'
import Background from '../../imgs/background.svg'
import Loader from '../parts/Loader/Loader'
import Recaptcha from 'react-google-recaptcha'
import { Container, Row, Col, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter, Button, Tooltip } from 'reactstrap'

import './formpage.css'

class RegisterForm extends Component {

    componentDidMount() {
   
        window.scrollTo(0, 0)
        axios.get('https://ipinfo.io/').then(res => {
            if(res.data) {
                this.setState({ip: res.data.ip})
                if(res.data.country) {
                    const ct = res.data.country
                    if(countries.filter(n => n=== ct).length === 1) {
                        this.setState({ location: ct, banned: true })
                    } else {
                        this.setState({ banned: false, country: ct })
                    }
                }
            }

        })
    }

    toggleTooltip() {
        this.setState({
          tooltip: !this.state.tooltip
        })
      }   
      
      toggleTooltipTwitter() {
        this.setState({
          tooltip: !this.state.tooltip_twitter
        })
      }   

      toggleTooltipIP() {
        this.setState({
          tooltip: !this.state.tooltip_ip
        })
      }  

      toggleModal() {
          this.setState({modal: true})
      }


      submitForm(e, close) {
        if(e) e.preventDefault()
        const { recaptcha, first_name, first_name_pristine, last_name, last_name_pristine, email, email_pristine, twitter, ether_address, ether_address_pristine, agreement, country, banned, ip, finished, newsletter, steemit } = this.state
        if(!first_name) this.setState({ first_name_pristine: false })
        if(!last_name) this.setState({ last_name_pristine: false })
        if(!recaptcha) this.setState({ recaptcha_pristine: false })
        if(!email) this.setState({ email_pristine: false })
        if(!ether_address) this.setState({ ether_address_pristine: false })
        if(ether_address.length !== 42) this.setState({ ether_address_pristine: false })
        if(!agreement) this.setState({ agreement_pristine: false})
        if(banned) {
            this.setState({global_warning: 'You cannot join this Token Challenge.', tooltip: true, submit: false})
            setTimeout(this.toggleTooltip, 2000)
        }
        if(first_name_pristine && last_name_pristine && email_pristine && ether_address_pristine && ether_address.length === 42 && agreement && recaptcha && !banned) {
            setTimeout(() => {
                if(!finished) {
                    this.setState({registration_failed: true, submit: false})
                }
            }, 1000 * 20)
            this.setState({ submit: true, finished: false })      
            axios.get(`https://obeliskai.herokuapp.com/api/register?first_name=${first_name}&last_name=${last_name}&email=${email}&ether_address=${ether_address}&twitter=${twitter}&country=${country}&ip=${ip}&group_id=${-1001132368851}&newsletter=${newsletter}&steemit=${steemit}`).then(res => {
                if(res.data.ok) {
                    this.setState({finished: true})
                    this.toggleModal()
                    setTimeout(() => close(), 20000)
                } else if(res.data.result === 'This email already exists') {
                    this.setState({global_warning: res.data.result, tooltip: true, submit: false, finished: true})
                    setTimeout(this.toggleTooltip, 3000)
                } else if(res.data.result === 'This twitter username already exists') {
                    this.setState({global_warning: res.data.result, tooltip_twitter: true, submit: false, finished: true})
                    setTimeout(this.toggleTooltipTwitter, 3000)
                } else if(res.data.result === 'You have already registered') {
                    this.setState({global_warning: res.data.result, tooltip_ip: true, submit: false, finished: true})
                    setTimeout(this.toggleTooltipIP, 3000)
                } else {
                    this.setState({registration_failed: true, submit: false, finished: true})
                }
            })
        }
    }

    constructor() {
        super()
        this.state = {
            first_name: '',
            first_name_pristine: true,
            last_name: '',
            last_name_pristine: true,
            email: '',
            email_pristine: true,
            steemit: '',
            steemit_pristine: true,
            gdpr: false,
            twitter: '',
            ip: '',
            recaptcha_pristine: true,
            newsletter: false,
            newsletter_pristine: true,
            twitter_pristine: true,
            ether_address: '',
            ether_address_pristine: true,
            agreement: false,
            agreement_pristine: true,
            recaptcha: false,
            submit: false,
            modal: false,
            banned: false,
            global_warning: '',
            tooltip: false,
            finished: false,
            tooltip_twitter: false
        }
        this.submitForm = this.submitForm.bind(this)
        this.toggleTooltip = this.toggleTooltip.bind(this)
        this.toggleTooltipTwitter = this.toggleTooltipTwitter.bind(this)
        this.toggleTooltipIP = this.toggleTooltipIP.bind(this)
    }

    render() {
        const { 
                first_name,
                first_name_pristine,
                last_name,
                gdpr,
                steemit, steemit_pristine,
                newsletter,
                newsletter_pristine,
                last_name_pristine, 
                email, email_pristine, 
                twitter, twitter_pristine, 
                ether_address, ether_address_pristine, 
                agreement, agreement_pristine,
                submit,
                banned,
                global_warning,
                tooltip,
                tooltip_ip,
                modal,
                tooltip_twitter,
                registration_failed,
                recaptcha_pristine,
                finished
            } = this.state
        return (
        <div className="background" >
            <div className="shopin_background_results" />
                <Modal isOpen={modal} toggle={this.toggle} className='form_container finished_modal'>
                <div className='form_container'>
                <ModalHeader className="justify-content-center">
                    <strong className="text_xl">Thank you for joining!</strong>
                </ModalHeader>
                        <ModalBody className="text-left">
                        <p className="text_black">Follow the link you have just received in your email address to complete your registration. <br/> <br/><strong>Make sure to check your spam box, and enjoy our Token Challenge!</strong></p>
                        </ModalBody>
                        <ModalFooter className="justify-content-center">
                            <a className="aa" href="https://t.me/shopineverywhere" target="_blank" rel="noopener"><Button type="submit" onClick={() => this.setState({finished: true})} className="button button_form button_finished">To Shopin's Telegram!</Button></a>
                        </ModalFooter>
                        </div>
                </Modal>
                <Modal isOpen={gdpr}  toggle={this.openGDPR} className='form_container gdpr_modal'>
                <div className='form_container text-white pointer text-center'>
                <ModalHeader className="justify-content-center">
                    <strong className="text_xl black text-center">Data Privacy Consent</strong>
                </ModalHeader>
                        <ModalBody className="text-left">
                            <p className="black justify_text">Shopin is going to receive your email address and store it in a mailing list. We will use your email only to send to you information regarding Shopin. We will treat your data in respect of international laws for Data Protection and Privacy (e.g. GDPR) and therefore: We won’t use your email address for a different purpose and we won’t sell or give-away your email address. We will respect your data rights, so you can ask to update or/and delete your email address from our mailing list by simply using the unsubscribe tool (contained in each email you will receive from us).<br/><br/> Do you agree? Please select “Yes” or “No”.</p>
                        </ModalBody>
                        <ModalFooter className="justify-content-center gdpr_footer">
                        <Button type="submit" onClick={() => {
                        this.setState({newsletter: true, gdpr: false})
                        }} 
                        className="button button_form button_gdpr">Yes</Button>
                            <Button type="submit" onClick={() => {
                                this.setState({newsletter: false, gdpr: false})
                                }} className="button button_form button_gdpr">No</Button>
                        </ModalFooter>
                        </div>
                </Modal>
                   <Tooltip placement="top" isOpen={tooltip} target="submit_button" >
                        {global_warning}
                    </Tooltip>
                    <Tooltip placement="top" isOpen={tooltip_twitter} target="twitter" >
                        {global_warning}
                    </Tooltip>
                    <Tooltip placement="top" isOpen={tooltip_ip} target="signup" >
                        {global_warning}
                    </Tooltip>
            <Navbar />
            <div className=" margin_top_50">
            <div className="justify-content-center text-center modal_head  margin_formpage_top">
                            <h2 className="form_heading formpage_header"><strong>{banned ? 'You cannot join.' :`Join Shopin's Token Challenge`} </strong></h2>
                        </div>
                    </div>
            <Container className="">
                    <Row className="text-center margin_top_30">
                        <Col xs="12" className="text-center">
                            <p className="form_instructions less_margin_top">{banned ? 'You country is banned from participating in the Token Challenge' : 'Fill in the form in order to join the airdrop'}</p>
                        </Col>
                    </Row>
                    {banned ? '' : <Form onSubmit={e => this.submitForm(e, this.props.close)}>
                        <Row className="form-row justify-content-center">
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">First Name</Label>
                                <Input
                                className="form_input"
                                value={first_name}
                                type="text"
                                id="signup" 
                                placeholder="First Name"
                                style={{borderColor: first_name_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({first_name_pristine: true })}
                                onBlur={() => this.setState({ first_name_pristine: first_name.length >=1 })}
                                onChange={e => this.setState({ first_name: e.target.value })} />
                                {first_name_pristine ? null : <small className="text_sm text_warning">Enter first name</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">Last Name</Label>
                                <Input
                                className="form_input"
                                value={last_name}
                                type="text"                                
                                placeholder="Last Name"
                                style={{borderColor: last_name_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({last_name_pristine: true })}
                                onBlur={() => this.setState({ last_name_pristine: last_name.length >=1 })}
                                onChange={e => this.setState({ last_name: e.target.value })} />
                                {last_name_pristine ? null : <small className="text_sm text_warning">Enter last name</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">Email</Label>
                                <Input
                                className="form_input"
                                value={email}
                                id="submit_button"
                                type="email"                                
                                placeholder="Email Address"
                                style={{borderColor: email_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({email_pristine: true })}
                                onBlur={() => this.setState({ email_pristine: isEmail(email) })}
                                onChange={e => this.setState({ email: e.target.value })} />
                                {email_pristine ? null : <small className="text_sm text_warning">Not an email address</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">Twitter username</Label>
                                <Input
                                className="form_input"
                                value={twitter}
                                type="text"
                                id="twitter"
                                placeholder="Twitter Username"
                                style={{borderColor: twitter_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({twitter_pristine: true })}
                                onBlur={() => this.setState({ twitter_pristine: true })}
                                onChange={e => this.setState({ twitter: e.target.value })} />
                                {twitter_pristine ? null : <small className="text_sm text_warning">Enter Twitter username</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">Steemit username</Label>
                                <Input
                                className="form_input"
                                value={steemit}
                                type="text"
                                id="steemit"
                                placeholder="Steemit Username"
                                style={{borderColor: steemit_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({steemit_pristine: true })}
                                onBlur={() => this.setState({ steemit_pristine: true })}
                                onChange={e => this.setState({ steemit: e.target.value })} />
                                {steemit_pristine ? null : <small className="text_sm text_warning">Enter Steemit username</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" md="10">
                                <FormGroup>
                                <Label className="form_label">Ether Address</Label>
                                <Input
                                className="form_input"
                                value={ether_address}
                                type="text"
                                placeholder="Ether Address"
                                style={{borderColor: ether_address_pristine ? '#4ccead' : '#cf2125'}}
                                onFocus={() => this.setState({ether_address_pristine: true })}
                                onBlur={() => this.setState({ ether_address_pristine: ether_address.length === 42 })}
                                onChange={e => this.setState({ ether_address: e.target.value })} />
                                {ether_address_pristine ? null : <small className="text_sm text_warning">Enter Ether address</small>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" className="text-center">
                                <FormGroup style={{marginBottom:'0px'}}>
                                <Label  check>
                            <Label className="form_label"></Label>
                                    <Input 
                                    className="form_checkbox"
                                    onChange={e => {
                                        this.setState({agreement: !agreement, agreement_pristine: !agreement ? true : agreement_pristine})
                                        
                                    }}
                                    type="checkbox" />
                                    </Label>
                                    <span className="check_box_text">I hereby accept the <a href={Terms} target="_blank" rel="noopener" className=" aa"><strong> Terms and Conditions</strong></a></span>
                                    {agreement_pristine ? null : <div><small className="text_sm text_warning checkbox_warning">Shopin's Token Challenge requires you to accept the Terms and Conditions</small></div>}
                                </FormGroup>
                            </Col>
                            <Col xs="11" className="text-center newsletter_checkbox">
                                <FormGroup style={{marginBottom:'0px'}}>
                                <Label  check>
                            <Label className="form_label"></Label>
                                    <Input 
                                    className="form_checkbox"
                                    onChange={e => {
                                        this.setState({newsletter: !newsletter,  gdpr: newsletter === true ? false : true})
                                    }}
                                    type="checkbox" checked={newsletter}/>
                                    </Label>
                                    <span className="check_box_text">Yes, I would like to receive updates via email.</span>
                                    {newsletter_pristine ? null : <div><small className="text_sm text_warning checkbox_warning">Shopin's Token Challenge requires you to accept the Terms and Conditions</small></div>}
                                </FormGroup>
                            </Col>
                            <Col xs="12" className="text-center center-recaptcha margin_top_20 captcha_col">
                            <Recaptcha
                                sitekey="6LcGHGoUAAAAAI_T3ma16oms994KLtcyny0Se3HU"
                                onChange={e => {
                                    this.setState({ recaptcha: true })
                                }}
                            />
                            </Col>
                            <Col xs="12" className="text-center">
                            {recaptcha_pristine ? '' : <p className="checkbox_warning">You must check the box to confirm that you are not a robot</p>}
                            </Col>
                            <Col xs="11" md="10">
                            {submit ? <Loader color={'#fff'} /> : <Button type="submit" className="button button_form white">Sign Up</Button>}
                            {registration_failed ? <p className="margin_top_20">Something went wrong, Please try again.</p> : null}
                            </Col>
                        </Row>
                    </Form>}

            </Container>
        <Footer/>
          </div>  
        )
    }
}

export default RegisterForm