import { GET_INPUT, GET_USERS, GET_OLD_URL } from './types'

export const sendToResults = users => async dispatch => {
  dispatch({ type: GET_USERS, payload: users })
}

export const sendOldURL = old_url => async dispatch => {
  dispatch({ type: GET_OLD_URL, payload: old_url })
}

export const input = input => async dispatch => {
    dispatch({ type: GET_INPUT, payload: input })
  }