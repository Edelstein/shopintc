import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'
import { connect } from 'react-redux'
import _ from 'lodash'
import axios from 'axios'
import * as actions from '../../actions'
import Footer from '../parts/Footer/Footer'
import { Redirect } from 'react-router-dom'
import Navbar from '../Navbar/Navbar'
import Result from './parts/Result'

import './results.css'
import Pattern from '../../imgs/pattern.png'
import BackgroundMobile from '../../imgs/backgroundmobile.svg'
import Search from '../../imgs/magnify.svg'
//Insert username to view your progress
class Results extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            found_user: false,
            modal: false
        }
      }
    renderSearch(is_phone) {
        const { value, clicked, tooltip } = this.state
         return is_phone ? null : <form 
          className="search searchbar_container_results"
          onSubmit={e => {
              e.preventDefault()
              this.props.input(value)
              if(!this.state.clicked && this.state.value) {
                  this.setState({ clicked: true, tooltip: false })
                  axios.get(`https://api.obelisk.ai/achievements/user/specific?user=${value}&group_id=-1001132368851`).then(res => {
                      this.setState({ clicked: false })
                      if(res) {
                          if(res.data.result === 'ok') {
                              const users = res.data.user.filter(n => {
                                  if(_.includes(n.groups, -1001132368851)) {
                                      return n
                                  }
                              })
                              const amount_of_users = users.length
                              if(amount_of_users === 0 ) {
                                  this.setState({ tooltip: true })
                              } else if(amount_of_users === 1) {
                                  this.setState({ found_user: true, link: users[0].id })
                              } else if(amount_of_users > 1) {
                                  this.props.sendToResults(users)
                                  this.setState({to_results: true})
                              }
                          } 
                      }
                  })
              }
  
          }}
          > 
              <input 
              value={value}
              onChange={e => this.setState({ value: e.target.value })}
              placeholder={is_phone ? 'Insert TG username' : 'Insert username to view your progress'}
              className="searchbar_results" 
              />
              <button type="submit" className="search_button" style={{backgroundColor: clicked ? '#737373' : ''}} id="tooltipbutton" ><img src={Search} /></button>
              {tooltip ? <div className="search_tooltip">No users found</div> : null}
          </form>
      }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize.bind(this))
        window.scrollTo(0, 0)
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize.bind(this))
    }
    render() {
        const { found_user, link } = this.state
        const is_phone = window.matchMedia("(max-width: 767px)").matches
        return (
            <div>
                <div className="shopin_background_results" />
            <Navbar />
                 <Container>
                 <Row className="text-center">
                        <Col xs="12">
                            {is_phone ? null : <h1 className="results_title_header">Track yourself</h1>}
                        </Col>
                        <Col xs="12">
                            {is_phone ? null : <p className="intro_text white" >Enter your telegram username here and we'll tell you how you're doing.</p>}
                            
                        </Col>
                    </Row>
                     <Row className="justify-content-center margin_top_180">
                        <Col md="9" xs="12">
                            {this.renderSearch(is_phone)}
                        </Col>
                     </Row>
                    <Row className="text-center margin_mobile_results">
                        {this.props.users ? this.props.users.length > 1 ? <Col xs="12">
                            <h2 className="results_title white less_margin">We found more than one member with this username:</h2>
                            <p className="green margin_top_20 white">{this.props.search}</p>
                        </Col> : null : <div className="results_no_users"/>}
                    </Row>
                    {this.props.users ? <Row className="justify-content-center results_container">
                        <Col xs="12" className="">
                            { this.props.users ? this.props.users.length > 1 ? this.props.users.map(n => <Result id={n.id} username={n.username} first_name={n.first_name} /> ) : null : null}
                        </Col>
                    </Row> : null}
                    {found_user ? <Redirect to={`/profile/${link}`} /> : null}
                    <Footer />

                </Container>
            </div>
        )
    }
    handleResize() {
        this.setState({width: window.innerWidth, height: window.innerHeight})
    }
}

function mapStateToProps({ search, users }) {
    return { search, users }
  }

export default connect(mapStateToProps, actions)(Results)