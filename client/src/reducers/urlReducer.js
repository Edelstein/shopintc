import { GET_OLD_URL } from '../actions/types'

export default function(state = null, action) {
  switch (action.type) {
    case GET_OLD_URL:
      return action.payload|| false;
    default:
      return state;
  }
}