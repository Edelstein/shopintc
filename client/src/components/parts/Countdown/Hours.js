import React, {Component} from 'react';
import moment from 'moment'
export default class Hours extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hours:0,
        };
    }
    
    componentWillMount() {
        this.getTimeUntil(this.props.deadline);
    }

    componentDidMount() {
        setInterval(()=>this.getTimeUntil(this.props.deadline),1000);
    }
    
    
    getTimeUntil(deadline){
        const time = moment(deadline).diff(moment());
        const hours = Math.floor((time/(1000*60*60))%24);
        this.setState({hours});
    }
    render() {
        return (
            <div>{this.state.hours > 0 ? this.state.hours <= 9 ? `0${this.state.hours}` : this.state.hours.toString() : '00'}</div>
        );
    }
}