import join from '../imgs/achievements/join.svg'
import quizbotwin from '../imgs/achievements/quizbotwin.svg'
import medium from '../imgs/achievements/medium.svg'
import advisor from '../imgs/achievements/advisor.svg'
import invitetotg from '../imgs/achievements/invitetotg.svg'
import jackpot from '../imgs/achievements/jackpot.svg'
import conversationstarter from '../imgs/achievements/conversationstarter.svg'
import quizbot from '../imgs/achievements/quizbot.svg'
import React from 'react'
import invitemoretotg from '../imgs/achievements/invitemoretotg.svg'
import twitter from '../imgs/achievements/twitter.svg'



const achievements = [
    {
        title: 'Joining The Community',
        link: 'https://t.me/shopineverywhere',
        text: 'Join the Telegram group! Engage with Shopin community until the end of the ICO.',
        img: join,
        reward: 10
    }, 
    {
        title: 'Inviting 3 Friends',
        link: '',
        text: 'Invite 3 friends to the Shopin’s Telegram community. Your friends must stay until the end of the ICO.',
        img: invitetotg ,
        reward: 50
    }, 
    {
        title: 'Inviting Additional 5 Members',
        text: 'Get 5 more friends in (8 in total)! Add them to Shopin’s Telegram and make sure they stay until the end of the challenge.',
        link: '',
        img: invitemoretotg,
        reward: 80
    }, 
    {
        title: 'The Advisor',
        link: '',
        text: 'Help a friend out! Answer another community member’s question in Telegram.',
        img: advisor,
        reward: 30
    }, 
    {
        title: 'Conversation Starter',
        link: '',
        text: 'Get a quality conversation going in Telegram by sending 10 messages back and forth with other users.',
        img: conversationstarter,
        reward: 40
    }, 
    {
        title: `Follow Shopin's Twitter`,
        link: 'https://twitter.com/shopinapp',
        text: 'Join us on Twitter for the latest Shopin news. Remember to stay as a follower until the end of the ICO.',
        img: twitter,
        reward: 30
    }, 
    {
        title: 'Medium Maximum Clap',
        link: 'https://medium.com/@ShopinApp',
        text: 'Max clap (50 times) Shopin’s 3 most recent blog posts',
        tooltip: 'and type /medium',
        img: medium,
        reward: 30
    }, 
    {
        title: `Participate in the Trivia`,
        link: '',
        text: 'Take part and play in the on-going trivia game in our Telegram group. You have 10 minutes for each question.',
        img: quizbot,
        reward: 30
    }, 
    {
        title: `Win in the Trivia`,
        link: '',
        text: `Participate and WIN in a trivia session on Shopin's Telegram group.`,
        img: quizbotwin,
        reward: 60
    }, 
    {
        title: 'Finish Everything',
        link: '',
        text: 'Complete all other achievements and you’ll automatically unlock this badge.',
        img: jackpot,
        reward: 100
    }
]

export default achievements