import React, {Component} from 'react';
import moment from 'moment'
export default class Minutes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minutes:0,
        };
    }
    
    componentWillMount() {
        this.getTimeUntil(this.props.deadline);
    }

    componentDidMount() {
        setInterval(()=>this.getTimeUntil(this.props.deadline),1000);
    }
    
    
    getTimeUntil(deadline){
        const time = moment(deadline).diff(moment());
         const minutes = Math.floor((time/1000/60)%60);
        this.setState({minutes});
    }
    render() {
        return (
            <div>{this.state.minutes > 0 ? this.state.minutes <= 9 ? `0${this.state.minutes}` : this.state.minutes.toString() : '00'}</div>
        );
    }
}