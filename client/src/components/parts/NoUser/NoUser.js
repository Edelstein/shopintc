import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import Navbar from '../../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './nouser.css'

import Background from '../../../imgs/background.svg'

const NoUser = props => <div className="background_results" >
        <Navbar />
        <div className="shopin_background_results" />
    <Container className="no_user_container">
        <Row className="text-center">
            <Col xs="12" className="margin_bottom_100">
                <h2 className="secondary_title white margin_top_180">This user is not participating in the Token Challenge</h2>
            </Col>
            <Col xs="12" className="margin_top_180">
                <Footer/>
            </Col>
        </Row>
    </Container>
</div>

export default NoUser