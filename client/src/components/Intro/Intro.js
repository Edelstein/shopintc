import React, { Component } from 'react'
import axios from 'axios'
import moment from 'moment-timezone'
import  { Link } from 'react-router-dom'
import Navbar from '../Navbar/Navbar'
import Footer from '../parts/Footer/Footer'
import Loader from '../parts/Loader/Loader'
import TransitionGroup from 'react-transition-group/TransitionGroup'
import Seconds from '../parts/Countdown/Seconds'
import CountUp from 'react-countup'
import Minutes from '../parts/Countdown/Minutes'
import Numbers from './parts/Numbers'
import Hours from '../parts/Countdown/Hours'
import Days from '../parts/Countdown/Days'
import { Container, Row, Col, Modal } from 'reactstrap'
import Form from '../Form/Form'
import ProgressBar from './parts/ProgressBar'
import Achievement from './parts/Achievement'
import AchievementNoAnimation from './parts/Achievement_No'
import achievements from '../../text/achievements'
import { detect } from 'detect-browser'
import Arrow from '../../imgs/arrow_hor.svg'
import One from '../../imgs/steps/first.svg'
import Two from '../../imgs/steps/second.svg'
import Three from '../../imgs/steps/third.svg'
import Four from '../../imgs/steps/fourth.svg'
import Background from '../../imgs/background.svg'
import Pattern from '../../imgs/pattern.png'
import BackgroundMobile from '../../imgs/backgroundmobile.svg'
import './intro.css'

class Intro extends Component {
    constructor() {
        super()
        this.state = {
            loading: true,
            total_tokens_animation: 0,
            tokens_collected: 0,
            total_tokens:  4081633,
            percentage: 0,
            modal: false
        }
        this.toggle = this.toggle.bind(this)
    }
    handleResize() {
        this.setState({width: window.innerWidth, height: window.innerHeight})
    }
    toggle() {
        this.setState({modal: !this.state.modal})
    }
// Achievement's IDS
// Join Telegram Channel = 0
// Community Builder = 1
// Community Constructor = 2
// Conversation Starter = 3
// Advisor = 4
// Message Streak = 5
// Twitter Follower = 6
// Facebook Follower = 7
// Instagram Follower = 8
// Quiz Bot = 9
// GetEm All = 10
// Coummunity Builder 10 Inviters = 11
// Conversation Starter 30 Messages = 12
// Custom-Low Value = 13
// Custom-Low Value = 14
// Multiple times achievement = 15
// Multiple times achievement = 16
// Custom-Medium Value = 17
// Custom-Medium Value = 18
// Custom-High Value = 19
// Custom-High Value = 20
// Custom-High Value = 21
// Medium = 22
// Particiapte In Quiz Bot = 23
    componentDidMount() {
        var oldURL = document.referrer;
        console.log(oldURL, 'hello')
        window.addEventListener("resize", this.handleResize.bind(this))
        window.scrollTo(0, 0)
        let truth = true
        const { total_tokens } = this.state
        axios.get('https://api.obelisk.ai/achievements/getlatesttotal?group_id=-1001132368851').then(res => {
            if(res.data.ok) {
                const sum = res.data.result
                // progress bar animation
                const juice = setInterval(() => {
                    if(truth) {
                        this.setState({ percentage: this.state.percentage + 0.1 })
                        if(this.state.percentage >= (sum/total_tokens) * 100) {
                            clearInterval(juice)
                        } 
                    } 
                }, 10)

                
                //
                this.setState({ loading: false, tokens_left: total_tokens - sum, tokens_collected: sum})
            }
        })
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize.bind(this))
    }

    render() {
        const browser = detect()
        const { percentage, tokens_left, loading, tokens_collected, total_tokens, modal } = this.state
        const is_phone = window.matchMedia("(max-width: 768px)").matches
        const is_small = window.matchMedia("(max-width: 992px)").matches
        return (
            <div className="background">
            <div className="shopin_background" />
                <Navbar intro={true} />
                <Container className="justify-content-center">
  
                <Modal isOpen={modal} toggle={this.toggle}>
                    <Form close={this.toggle} is_phone={is_phone} />
                </Modal>
                    <Row className="text-center">
                        <Col xs="12">
                            <h1 className="main_title">Token Challenge</h1>
                        </Col>
                        <Col xs="12" className="justify-content-center">
                            <p className="intro_text white" >Welcome to Shopin's Token Challenge. Join our community and unlock your rewards.</p>
                            
                        </Col>
                    </Row>
                    <div className="white_wrapper">
                    <Row className="justify-content-center countdown">
                            <span className="countdown_time_container blue">
                                <Days deadline={moment('2018-09-09').tz('Asia/Jerusalem')} />
                                <p className="countdown_detail">Days</p> 
                            </span>
                            <span className="countdown_seperator blue">:</span>
                            <span className="countdown_time_container blue">
                                <Hours deadline={moment('2018-09-09').tz('Asia/Jerusalem')} />
                                <p className="countdown_detail">Hours</p>
                            </span>
                            <span className="countdown_seperator blue">:</span>
                            <span className="countdown_time_container blue">
                                <Minutes deadline={moment('2018-09-09').tz('Asia/Jerusalem')} />
                                <p className="countdown_detail">Minutes</p> 
                            </span>
                            <span className="countdown_seperator blue">:</span>
                            <span className="countdown_time_container blue">
                                <Seconds deadline={moment('2018-09-09').tz('Asia/Jerusalem')} />
                                <p className="countdown_detail">Seconds</p>
                            </span>
                    </Row>
                    <Row className="mobile_visible text-center margin_top_30">
                        <Col xs="12" className="total_tokens_mobile"><p >Total Tokens: <CountUp separator=',' start={0} end={total_tokens} /></p></Col>
                    </Row>
                    {loading ? <Loader /> : <ProgressBar is_phone={is_phone} percentage={percentage} />}
                    {loading ? null : <Numbers total_tokens={total_tokens} is_phone={is_phone}  tokens_left={tokens_left} tokens_collected={tokens_collected} />}
                    </div>
                    <Row className=" steps_container mobile_steps">
                        <div className="center_col first_of_steps">
                            <div className="mobile_invisible steps_start margin_top_80 text-center">
                                <img src={One} className="steps  complete_achievements" />
                                <p className="steps_bold margin_top_ blue limit_width">Join Token Challenge</p>
                                {is_phone ? <Link to='/form' className="aa margin_top_30 button_reverse button_md_mobile white">Sign Up</Link> : <div onClick={() => {
                                    this.toggle()
                                }} className="margin_top_30 button_reverse button_md_mobile ">Sign Up</div>}
                            </div>
                        </div>
                        <div className="text-center center_col second_step margin_top_80 text-center">
                            <img src={Two} className="steps complete_achievements" />
                            <p className="steps_bold  blue limit_width"> Join Telegram Group</p>
                            <a href="https://t.me/shopineverywhere" target="_blank" rel="noopener" className="button aa margin_top_30 button_md_mobile ">Join Telegram</a>
                        </div>
                        <div className="text-center center_col third_step margin_top_80 text-center">
                            <img src={Three} className="steps complete_achievements" />
                            <p className="steps_bold  blue limit_width">Complete the achievements </p>
                            <Link to="/results" className="button aa margin_top_30 button_md_mobile b">Track yourself</Link>
                        </div>
                        <div className="text-center end_of_steps margin_top_80 text-center">
                            <img src={Four} className="steps complete_achievements" />
                            <p className="steps_bold  blue limit_width"> Get your tokens </p>
                        </div>
                    </Row>
                    <Row className="text-center  justify-content-center drag_bottom_intro">
                        <Col xs="12" className=" intro_achievements_margins">
                            <h2><strong className="secondary_title ">Achievements</strong></h2>
                        </Col>
                    </Row>
                   {browser.name === 'chrome' || browser.name === 'firefox' || browser.name === 'edge' ? <TransitionGroup>
                <div className="achievements_container">
                        {achievements.map((n, i) => <Achievement key={i} tooltip={n.tooltip} is_phone={is_phone} title={n.title} evan={n.evan} first={n.first} link={n.link} sub_title={n.sub_title} text={n.text} reward={n.reward} img={n.img} />)}
                </div>
                </TransitionGroup> :<div className="achievements_container">
                        {achievements.map((n, i) => <AchievementNoAnimation key={i} tooltip={n.tooltip} is_phone={is_phone} title={n.title} evan={n.evan} first={n.first} link={n.link} sub_title={n.sub_title} text={n.text} reward={n.reward} img={n.img} />)}
                </div>} 
                <Footer />
                
                </Container>

            </div>
        )
    }
}

export default Intro