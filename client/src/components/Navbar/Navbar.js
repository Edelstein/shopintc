import React, { Component } from 'react'
import Shopin from '../../imgs/logo.svg'
import Hamburger from '../../imgs/nav/menu.svg'
import Modal from 'react-modal'
import { detect } from 'detect-browser'
import X from '../../imgs/x.svg'
import Blur from '../../imgs/blur.svg'
import Close from '../../imgs/nav/close.svg'
import { Container, Collapse,
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    Tooltip
} from 'reactstrap'
import Search from '../../imgs/magnify.svg'
import _ from 'lodash'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import './navbar.css'

const customStyles = {
    content : {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      width: '100%',
      height: '100%',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      backgroundColor: 'transparent',
      border: 'none'
    },
    overlay: {
        zIndex: 1
    }
  }
class NavigationBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            found_user: false,
            clicked: false,
            tooltip: false,
            modal: false,
            tooltip_helpbot: false
        }
        this.toggle = this.toggle.bind(this)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.toggleTool = this.toggleTool.bind(this)
      }
      componentDidMount() {
          
          this.closeModal()
      }

    renderSearch() {
      const { value, clicked, tooltip } = this.state
       return <form 
        className="search searchbar_container_mobile"
        onSubmit={e => {
            e.preventDefault()
            this.props.input(value)
            if(!this.state.clicked && this.state.value) {
                this.setState({ clicked: true, tooltip: false })
                axios.get(`https://api.obelisk.ai/achievements/user/specific?user=${value}&group_id=-1001132368851`).then(res => {
                    this.setState({ clicked: false })
                    if(res) {
                        if(res.data.result === 'ok') {
                            const users = res.data.user.filter(n => {
                                if(_.includes(n.groups, -1001132368851)) {
                                    return n
                                }
                            })
                            const amount_of_users = users.length
                            if(amount_of_users === 0 ) {
                                this.setState({ tooltip: true })
                            } else if(amount_of_users === 1) {
                                this.setState({ found_user: true, link: users[0].id })
                            } else if(amount_of_users > 1) {
                                this.props.sendToResults(users)
                                this.setState({to_results: true})
                            }
                        } 
                    }
                })
            }

        }}
        >        {this.state.modal ? null : <img src={Hamburger} className="collapse_nav mobile_visible" onClick={this.openModal} />}
                
            <input 
            value={value}
            onChange={e => this.setState({ value: e.target.value })}
            placeholder="Insert TG username" 
            className="searchbar" 
            />
            <button type="submit" className="search_button" style={{backgroundColor: clicked ? '#737373' : ''}} id="tooltipbutton" ><img src={Search} /></button>
            
        </form>
    }

  openModal() {
    this.setState({modal: true});
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }
  toggleTool() {
    this.setState({
      tooltip_helpbot: !this.state.tooltip_helpbot
    })
  }
  closeModal() {
    this.setState({modal: false});
  }
  render() {
      const browser = detect()
      const ie = browser.name === 'ie'
      const { found_user, link, to_results, modal, tooltip, tooltip_helpbot } = this.state
    return (
        <div className="navbar_container">

        <Modal
          isOpen={modal}
          onRequestClose={this.closeModal}
          style={customStyles}
        >
          <img onClick={this.closeModal} src={X} className="close_menu_button" />
          <div className="mobile_menu">
          <a href='https://shopin.com' target="_blank" rel="noopener" onClick={this.closeModal} className="aa white">Website</a>
          <div className="mobile_nav_seperator" />
          <div className="text-center"><Link className="aa white" to='/help'>Help</Link></div>


        </div>
        </Modal>
        <Navbar className="navbar_container" expand="md">
        <Container>
        <div className="mobile_invisible">
            <Link to="/results" className="button aa margin_top_30 button_navbar">Track yourself</Link>
        </div>
          <NavbarBrand className="navbar-header" href="/">  
            <div>
                <Link to="/"><img src={Shopin} alt="Shopin" className={`logo ${ie ? 'logo_explorer' : ''}`} /></Link>
                {found_user ? <Redirect to={`/profile/${link}`} /> : null}
                {to_results ? <Redirect to='/results' /> : null}
            </div>
        </NavbarBrand>
        <div className="mobile_visible max_width">
            {this.renderSearch()}
            {tooltip ? <span className="search_tooltip">No users found</span> : null}
        </div>
        {this.state.modal ? null : <img src={Hamburger} className="collapse_nav_ipad " onClick={this.openModal} />}
          <Collapse isOpen={false} className="" navbar>
            <Nav className="nav_elements" navbar>
            <NavItem className={`nav_link ${ie ? '' : 'less_width'}`}>
            <a href='https://shopin.com' target="_blank" rel="noopener" onClick={this.closeModal} className="aa button_reverse button_navbar_home">Website</a>
              </NavItem>
              <NavItem className={`nav_link ${ie ? '' : 'less_width'}`}>
                <Link className="aa" to='/help'>Help</Link>
              </NavItem>
            </Nav>
          </Collapse>
          </Container>
        </Navbar>
      </div>
    )
  }
}


  

export default connect(null, actions)(NavigationBar)