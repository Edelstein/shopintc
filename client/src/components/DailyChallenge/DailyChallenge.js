import React, { Component } from 'react'
import { Container, Row, Col, Modal } from 'reactstrap'
import { Link } from 'react-router-dom'
import Battle from '../../imgs/tradingchallenge.svg'
import Navbar from '../Navbar/Navbar'
import Form from '../Form/Form'
import Footer from '../parts/Footer/Footer'
import Background from '../../imgs/background.svg'
import BackgroundMobile from '../../imgs/backgroundmobile.svg'
import Pentagon from '../../imgs/pentagon.svg'

import './dailychallenge.css'

class DailyChallenge extends Component {
    componentDidMount() {
        window.addEventListener("resize", this.handleResize.bind(this))
        window.scrollTo(0, 0)
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize.bind(this))
    }
    constructor() {
        super()
        this.state = {
            modal: false
         
        }
        this.toggle = this.toggle.bind(this)
    }

    toggle() {
        this.setState({modal: !this.state.modal})
    }
    render() {
        const { modal } = this.state
        const is_phone = window.matchMedia("(max-width: 768px)").matches
        return (
            <div className="background_results" style={{backgroundImage: `url('${is_phone ? BackgroundMobile : Background}')`}}>
                <Navbar dailychallenge={true} />
                <Container >
                    <Modal isOpen={modal} toggle={this.toggle}>
                        <Form close={this.toggle} />
                    </Modal>
                    <Row className="text-center justify-content-center">
                        <Col xs="12" className="margin_top_battle">
                            <img src={Battle}  className="battling_animals"/>
                        </Col>
                        <Col xs="12">
                            <h1 className="green margin_top_50"><strong>TRADING</strong> <span className="font_normalize" >CHALLENGE</span></h1>
                        </Col>
                        <Col xs="12">
                            <p className="intro_text" >So, you think you can trade? Put your trading skills to the test.
                            </p>
                        </Col>
                        <Col xs="12" className="center_col">{is_phone ? <Link to="/form" className="button_reverse aa margin_top_30 start_here">Start here</Link> : <div onClick={this.toggle} className="button_reverse aa margin_top_30 ">Start here</div>}</Col>
                    </Row>
                    <Row className="text-left margin_top_40 text-md-left text-center">
                        <Col xs="12">
                            <h2 className="text_xl best_trader small_margin_bottom_mobile">Enter our trading contest </h2>
                        </Col>
                        <Col xs="12" >
                            <p className="intro_text daily_font ">You’re a super savvy trader, and you know it. You keep one eye on the news, and the other on the market. You always follow the signals and you’re all about educated trades. You’re the one friends go to for trading advice, and half of them ask if they can just hand you their money to play with (but who needs that kind of pressure?).</p>
                        </Col>

                    </Row>
                    <Row className="margin_top_50 margin_bottom_60 text-md-left text-center">
                        <Col xs="8" className="center_on_mobile">
                            <div className="green_line" />
                        </Col>
                    </Row>
                    <Row className="text-md-left text-center battle_size">
                        <Col xs="12">
                            <p className="battle_size">Each day, one Capitalise trader will win the pot, based on the 24-hour ROI of their strategy. As you probably guessed, the trader with the highest positive ROI will take first prize. That prize will be equal to the nominal profit of the winning strategy, up to the prize pot amount.</p>
                            <p className="battle_size">The daily win will be subtracted from the prize pot total. But to be sure that there’s always something at stake, we’ll increase the prize pot by $200 USD-worth of tokens every day.</p>
                            <p className="battle_size">Once the prize is set and won, we’ll publish the winner’s Telegram name and amount.</p>
                            <p className="battle_size">To make a sweet deal even sweeter, we’ll be distributing the prize in CAP Tokens after our Token Generation Event — so you’ll be among the first CAP Token holders.</p>
                        </Col>
                    </Row>
                    <Row className="margin_top_50 margin_bottom_60 text-md-left text-center">
                        <Col xs="8" className="center_on_mobile">
                            <div className="green_line" />
                        </Col>
                    </Row>
                    <Row className="justify-content-md-start text-md-left text-center margin_top_60  text-center justify-content-center">
                        <Col xs="12">
                        <h2 className="daily_subtitle">Like any good contest, we have a couple of rules. </h2>
                        </Col>
                        <Col md="8" xs="10" className="daily_rule_container">
                            <div style={{backgroundImage: `url(${Pentagon})`}} className="polygon" >1</div>
                            <p className="text_md daily_rule">Only real money strategies are eligible for the contest. That’s right.<br/> Put your money where you mouth is and show us all how it’s done.</p>
                        </Col>
                        <Col md="8" xs="10" className="daily_rule_container">
                            <div style={{backgroundImage: `url(${Pentagon})`}} className="polygon" >2</div>
                            <p className="text_md daily_rule">The contest is for Capitalise Airdrop participants only.<br/> But good news: it’s easy to participate in airdrop.</p>
                        </Col>
                        <Col xs="12" className="text-center margin_top_40 "><h2 className="text_xl best_trader">May the best trader win!</h2></Col>
                        <Col xs="12" className="center_col text-center ">{is_phone ? <Link to="/form" className="button_reverse aa margin_top_30 start_here">Start here</Link> : <div onClick={this.toggle} className="button_reverse aa margin_top_30 ">Start here</div>}</Col>
                    </Row>
                    <Footer/>
                </Container>
            </div>
        )
    }
    handleResize() {
        this.setState({width: window.innerWidth, height: window.innerHeight})
    }
}

export default DailyChallenge