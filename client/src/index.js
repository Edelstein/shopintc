import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import Intro from './components/Intro/Intro'
import Results from './components/Results/Results'
import FormPage from './components/FormPage/FormPage'
import Help from './components/Help/Help'
import DailyChallenge from './components/DailyChallenge/DailyChallenge'
import NoUser from './components/parts/NoUser/NoUser'
import Profile from './components/Profile/Profile'
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import './tools.css'
import './backgrounds.css'

import reducers from './reducers'

const store = createStore(reducers, {}, applyMiddleware(reduxThunk));


ReactDOM.render(
<Provider store={store}>
    <Router>
        <Switch>
            <Route path='/profile/:id' component={Profile} />
            <Route path='/results' component={Results} />
            <Route path='/form' component={FormPage} />
            <Route path='/help' component={Help} />
            <Route path='/tradingchallenge' component={DailyChallenge} />
            <Route path='/nouser' component={NoUser} />
            <Route path='/' component={Intro} />
        </Switch>
    </Router>
</Provider>,
 document.getElementById('root'))

