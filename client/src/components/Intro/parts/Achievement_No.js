import React, { Component } from 'react'
import Token from '../../../imgs/reward.svg'
import Mark from '../../../imgs/tooltip.svg'
import Box from '../../../imgs/tooltipbox.svg'
class Achievement extends Component {
    constructor() {
        super()
        this.state = {
            facebook_tooltip: false
        }
    }
    render() {
        const { facebook_tooltip } = this.state
        return (
            <div className="achievement margin_top_100">
                    <div className="center_col margin_achievement_image">
                        <img src={this.props.img} className={`achievement_img_container`} />
                        {this.props.is_phone ? '' : <p className="achievement_reward mobile_invisible"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {this.props.reward} CSP</p>}
                    </div>
                    <div className="achievement_text_container" >
                    {this.props.tooltip ? <div>
                            <img onClick={() => this.setState({facebook_tooltip: !this.state.facebook_tooltip})} src={Mark} className="facebook_tooltip" />
                            {facebook_tooltip ? <div  className="facebook_tooltip_box" style={{backgroundImage: `url('${Box}')`}} >
                            <span className="facebook_tooltip_text">Go to the <a href="https://t.me/crowdnetworkbot?start=Shopin" target="_blank" rel="noopener">Telegram Bot</a> {this.props.tooltip}</span>
                            </div> : null}
                            </div>: null}
                    <div className="achievement_title_background">
                        {this.props.link ? <a href={this.props.link} className="achievement_anchor" target="_blank" rel="noopener"><h2 className="achievement_title  ">{this.props.title}</h2></a>: <h2 className="achievement_title  ">{this.props.title}</h2>}
                        </div>
                        <p className="achievement_text opensans">{this.props.text}</p>
                        {this.props.is_phone ? <p className="achievement_reward"><span className="reward">Reward</span><br/> <img src={Token} className="achievement_token_reward"/> {this.props.reward} CSP</p> : ''}
                    </div>
            </div>
        )
    }
}

export default Achievement