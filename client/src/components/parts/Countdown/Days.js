import React, {Component} from 'react'
import moment from 'moment'
export default class Days extends Component {
    constructor(props) {
        super(props);
        this.state = {
            days:0,
        }
    }
    
    componentWillMount() {
        this.getTimeUntil(this.props.deadline);
    }

    componentDidMount() {
        setInterval(()=>this.getTimeUntil(this.props.deadline),1000);
    }
    
    
    getTimeUntil(deadline){
        const time = moment(deadline).diff(moment());
        const days = Math.floor((time/(1000*60*60*24)));
        this.setState({days})
    }
    render() {
        return (
            
            <div>{this.state.days > 0 ? this.state.days <= 9 ? `0${this.state.days}` : this.state.days.toString() : '00'}</div>
        )
    }
}