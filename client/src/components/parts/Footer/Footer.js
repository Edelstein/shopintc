import React from 'react'
import { Row, Col } from 'reactstrap'
import Terms from '../../../docs/terms.pdf'
import './footer.css'

const Footer = props => <Row className="text-center footer_container">
    <Col xs="12">
        <p className="text_sm footer_anchor footer_color">Powered by: <a className="aa" target="_blank" rel="noopener" href="https://crowd.network">Crowd.Network</a> | <a href={Terms} className="aa" target="_blank"><strong>Terms and Conditions</strong></a></p>
    </Col>
</Row>

export default Footer 