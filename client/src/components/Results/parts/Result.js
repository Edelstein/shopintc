import React, { Component } from 'react'
import axios from 'axios'
import _ from 'lodash'
import Default from '../../../imgs/profile/noimg.svg'
import Task from '../../../imgs/profile/tasks.svg'
import Coin from '../../../imgs/profile/tokens.svg'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Row, Col } from 'reactstrap'
class Result extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: 'Loading',
            first_name: 'error'
        }
    }
    componentDidMount() {
        let sum = 0
        axios.get(`https://api.obelisk.ai/api/users/userphotoid?id=${this.props.id}&group_id=-1001132368851`).then(res => {
            if(res.data) {
                if(res.data.ok) {
                    this.setState({ image: res.data.result.file_id })
                } else {
                    this.setState({ image: false })
                }
            }
        })
        axios.get(`https://api.obelisk.ai/achievements/user/all?user_id=${this.props.id}&group_id=-1001132368851`).then(res => {
            if(res.data.result === 'ok') {
                const date = res.data.user_achievements.filter(n => n.achievement === 0)
                if(date.length === 1) {
                    this.setState({join_date: date[0].created_at})
                }
                res.data.user_achievements.map(n => {
                    switch(n.achievement) {
                        case 0:
                            return sum += 10
                        case 1:
                            return sum += 50
                        case 2:
                            return sum += 80
                        case 4:
                            return sum += 30
                        case 3:
                            return sum += 40
                        case 6:
                            return sum += 30
                        case 22:
                            return sum += 30
                        case 23:
                            return sum += 30
                        case 9:
                            return sum += 60
                        case 10:
                            return sum += 100
                        default:
                            return 0
                    }
                })
        this.setState({ sum: sum })

                this.setState({ tasks: res.data.user_achievements.length, tokens: sum, achievements: res.data.user_achievements.map(n=> n.achievement) })

            } else if (res.data.result === 'bad') {
                this.setState({ no_achievs: true })
            }
        })
    }
    render() {
        const { sum, join_date, image, tasks } = this.state
        return (
            <Link className="aa" to={`/profile/${this.props.id}`}>
            <Row className="result_container justify-content-center text-center">
                <Col md="4" lg="1"><img src={image ? `http://images.obelisk.ai:3000/imgs/${image}.jpg` : Default} className="results_img margin_top_30" /></Col>
                <Col md="4" lg="3" className="result_user_container margin_top_30"><strong className="result_username">{this.props.username ? `@${this.props.username}` : this.props.first_name} </strong><span className="result_line" /></Col>
                <Col md="12" lg="4" className="result_user_container margin_top_30 mobile_invisible"><span>Member since: <strong>{moment(join_date).format('DD/MM/YY')}</strong></span> <span className="result_line" /></Col>
                <Col md="4" xs="6" lg="2" className="result_user_container margin_top_30 coin_result_container"><span><img src={Coin} className="coin_result" /></span><p className="coin_result_detail">{sum}</p><span className="result_line end_result_line" /></Col>
                <Col md="4" xs="6" lg="2" className="result_user_container margin_top_30 coin_result_container"><span><img src={Task} className="coin_result" /></span><p className="coin_result_detail">{tasks}</p></Col>
                <div className="mobile_seperator_results" />
            </Row>
                
            
            </Link>
        )
    }
}

export default Result